#!/usr/bin/env groovy

def call(Map param) {
    def imageName = param.get('imageName') as String

    pipeline {
        agent any
        environment {
            registryCredential = 'hifra-dockerhub'
        }
        stages {
            stage('Build image') {
                steps {
                    script {
                        props = readJSON file:'package.json'

                        dockerImage = docker.build "${imageName}"

                        // sh "docker build -t '${imageName}:${props.version}' ."
                    }
                }
            }

            stage('Push image to Docker Hub') {
                steps {
                    script {
                        docker.withRegistry('', registryCredential) {
                            dockerImage.push("${props.version}")
                            dockerImage.push("latest")
                        }
                    }
                }
            }

            stage('Delete unused image from worker') {
                steps {
                    script {
                        sh "docker rmi ${imageName}:${props.version}"
                        sh "docker rmi ${imageName}:latest"
                    }
                }
            }
        }
    }
}
